<?php

/**
 * @file
 * FacetAPI Field - Widget
 *
 * @author amorsent
 */

/**
 * Implementation of hook_widget_info().
 */
function facetapi_field_widget_info() {
  return array(
    'facetapi_field_widget' => array(
      'label' => t('Faceted search parameters'),
      'field types' => array('facetapi_field'),
//      'multiple values' => CONTENT_HANDLE_CORE,
//      'callbacks' => array(
//        'default value' => CONTENT_CALLBACK_DEFAULT,
//      ),
    ),
  );
}

/**
 * Implementation of hook_widget_settings().
 */
//function facetapi_field_widget_settings($op, $widget) {
//  switch ($op) {
//    case 'form':
//      $form = array();
//
//      if ($widget['type'] == 'facetapi_field_widget') {
//
//      }
//
//      return $form;
//
//    case 'save':
//      return array();
//  }
//}

/**
 * Implementation of hook_widget().
 *
 * Attach a single form element to the form. It will be built out and
 * validated in the callback(s) listed in hook_elements. We build it
 * out in the callbacks rather than here in hook_widget so it can be
 * plugged into any module that can provide it with valid
 * $field information.
 *
 * Content module will set the weight, field name and delta values
 * for each form element. This is a change from earlier CCK versions
 * where the widget managed its own multiple values.
 *
 * If there are multiple values for this field, the content module will
 * call this function as many times as needed.
 *
 * @param $form
 *   the entire form array, $form['#node'] holds node information
 * @param $form_state
 *   the form_state, $form_state['values'][$field['field_name']]
 *   holds the field's form values.
 * @param $field
 *   the field array
 * @param $items
 *   array of default values for this field
 * @param $delta
 *   the order of this item in the array of subelements (0, 1, 2, etc)
 *
 * @return
 *   the form item for a single element for this field
 */
function facetapi_field_widget(&$form, &$form_state, $field, $items, $delta = 0) {
  $element = array(
    '#type' => $field['widget']['type'],
    '#default_value' => isset($items[$delta]) ? $items[$delta] : '',
  );
  return $element;
}

/**
 * Process an individual element.
 *
 * Build the form element. When creating a form using FAPI #process,
 * note that $element['#value'] is already set.
 *
 * The $fields array is in $form['#field_info'][$element['#field_name']].
 */
function facetapi_field_widget_process($element, $edit, $form_state, $form) {
  $field = $form['#field_info'][$element['#field_name']];
  $delta = $element['#delta'];
  $type_name = $element['#type_name'];
  $field_name = $element['#field_name'];
  
  // Add resources
  $path = drupal_get_path('module', 'facetapi_field');
  drupal_add_css($path . '/facetapi_field.css');
  drupal_add_js($path . '/facetapi_field.js');

  // Build iframe url
  $query['f'] = $element['#value']['data'];
  $iframe_url =  implode('/', array('facetapi-field', $type_name, $field_name, $delta));
  $iframe_url = url($iframe_url, array('query' => $query));

  // Set the element theme
  $element['#theme'] = 'facetapi_field_widget_item';

  // The data field is our actual stored information
  $element['data'] = array(
    '#tree' => 'true',
    '#type' => 'value',
    '#default_value' => isset($element['#value']['data']) ? $element['#value']['data'] : NULL,
  );
  
  // The url field will be populated by javascript to indicate the iframe's current url
  $element['url'] = array(
    '#type' => 'hidden',
    '#attributes' => array('class' => 'facetapi-field-url'),
    '#default_value' => '',
  );

  // The iframe allows us to browse the facets
  $element['iframe'] = array(
    '#type' => 'markup',
    '#value' => '<iframe class="facetapi-field-iframe" src="' . $iframe_url .'"></iframe>',
  );

  // Used so that hook_field('validate') knows where to flag an error.
//  $element['_error_element'] = array(
//    '#type' => 'value',
//    '#value' => implode('][', array_merge($element['#parents'], array($field_key))),
//  );
  
  if (empty($element['#element_validate'])) {
    $element['#element_validate'] = array();
  }
  array_unshift($element['#element_validate'], 'facetapi_field_widget_validate');

  return $element;
}

/**
 * Validate a facetapi_field_widget element.
 */
function facetapi_field_widget_validate($element, &$form_state) {
  $url = parse_url($element['url']['#value']);

  // If the form is submitted quickly, before the iframe has had time to load,
  // then the javascript may have set an invalid path, such as 'about:blank'
  // here we only set new data if the path appears valid.
  if (substr($url['path'], 1, 14) == 'facetapi-field') {
    parse_str($url['query'], $data);
    $data = !empty($data['f']) ? $data['f'] : array();
    sort($data);

    // @todo: We should validate the facet options, it's possible that it could be faked
    // which is potentially a security issue ??
    form_set_value($element['data'], $data, $form_state);
  }
  else {
    // If the path is not valid, then the user must have edited some other
    // field very quickly and submitted the form before the iframe loaded.
    // We'll assume no change to this field and maintain the previous value
    form_set_value($element['data'], $element['#default_value']['data'], $form_state);
  }
}

/* ************************************************************************
 * Widget Theme Functions
 * ************************************************************************/

/**
 * FAPI theme for an individual FacetAPI Field element.
 */
function theme_facetapi_field_widget($element) {
  $element['#collapsible'] = TRUE;
  unset($element['#value']);
  return theme('fieldset', $element);
}

/**
 * FAPI theme for an individual FacetAPI Field item element.
 */
function theme_facetapi_field_widget_item($element) {
  return $element['#children'];
}