<?php

/**
 * @file
 * FacetAPI Field - Menu Callbacks
 */

/**
 * Menu callback for search iframe
 */
function facetapi_field_iframe($type_name, $field_name, $delta) {
  drupal_add_css(drupal_get_path('module', 'facetapi_field') . '/facetapi_field.css');
  
  $field = content_fields($field_name, $type_name);
  $searcher = $field['searcher'];
  //$adapter = facetapi_adapter_load($searcher);

  // This is specific to apache solr but in necessary to bootstrap the search facets
  // @todo: Does facetapi provide a more general way to do this ?
  $page_id = 'core_search';
  $search_page = apachesolr_search_page_load($page_id);
  // revise search base path to the current url
  $search_page['search_path'] = implode('/', array('facetapi-field', $type_name, $field_name, $delta)); 
  $conditions = apachesolr_search_conditions_default($search_page);
  $results = apachesolr_search_search_results($keys, $conditions, $search_page);

  // Generate the facet blocks via facet api
  $realm_name = 'block';
  module_load_include('inc', 'facetapi', 'facetapi.block');
  $elements = facetapi_build_realm($searcher, $realm_name);
  foreach (element_children($elements) as $key) {
    $delta = "facetapi_{$key}";
    // @todo: order/filter these pseudo-blocks according to block.module weight, visibility (see 7.x-1beta4)
    $block = new stdClass();
    $block->visibility = TRUE;
    $block->enabled = TRUE;
    $block->module = 'facetapi';
    $block->subject = theme('facetapi_title', array('title' => $elements[$key]['#title']));
    //$build[$delta] = $elements[$key];
    $block->region = NULL;
    $block->delta = 'apachesolr-' . $key;
    $block->content = $elements[$key][$key];
    if (!empty($block->content)) {
      $blocks[$delta] = $block;
    }
  }
  
  // Current search
//  foreach ($elements['#adapter']->getAllActiveItems() as $item) {
//    $facet_name = $item['facets'][0];
//    $facet_value = $item['value'];
//    $data[$facet_name][$facet_value] = $item;
//  }
//  dpm($data);
  
  
  // Theme the blocks
  $output = '<div id="facetapi-field-filters">';
  foreach ($blocks as $facet_field => $block) {
    $output .= theme('block', $block);
  }
  
  $output .= '</div><div id="facetapi-field-results">';
  
  // Build our page and allow modification.
  $build_results = apachesolr_search_search_page_custom($results, $search_page, $build);

  $build_output = NULL;
  foreach($build_results as $build_result) {
    $output .= $build_result;
  }
  $output .= '</div>';
  
  return $output;

//  return theme('apachesolr_search_browse_blocks', $blocks);
  
//  
//  $page_id = 'core_search';
//  $search_page = apachesolr_search_page_load($page_id);
//  $search_page['search_path'] = $_GET['q'];
//  //dpm($search_page);
//
//  // Retrieve the conditions that apply to this page
//  $conditions = apachesolr_search_conditions_default($search_page);
//  // Retrieve the results of the search
//  $results = apachesolr_search_search_results($keys, $conditions, $search_page);
////  // Initiate our build array
////  $build = array();
////  // Add a custom search form if required
////  if (!empty($search_page['settings']['apachesolr_search_search_box'])) {
////    // Adds the search form to the page.
////    $build['search_form'] = drupal_get_form('apachesolr_search_custom_page_search_form', $search_page, $keys);
////  }
//  // Build our page and allow modification.
//  //$build_results = apachesolr_search_search_page_custom($results, $search_page, $build);
//  
////  $build_output = NULL;
////  foreach($build_results as $build_result) {
////    $build_output .= $build_result;
////  }
//  
//  $build_output = apachesolr_search_page_browse('browse', $search_page['env_id']);
//  
//  echo $build_output;




//  $adapter = facetapi_adapter_load('apachesolr@solr');
//
//
//  // Adds active facets to the current search block.
////  $searcher = $adapter->getSearcher();
//  foreach ($adapter->getAllActiveItems() as $item) {
//    $facet_name = $item['facets'][0];
//    $facet_value = $item['value'];
//    $data[$facet_name][$facet_value] = $item;
//  }
//
//
////  $adapter = facetapi_adapter_load('apachesolr@solr');
////  dpm($adapter);
//
////  module_load_include('inc', 'apachesolr_search', 'apachesolr_search.pages');
////  echo apachesolr_search_custom_page($page_id);
////  echo drupal_get_css();
////  echo drupal_get_js();
//  
//  //$script = 'parent.Drupal.facetapi_field_set_data(\''.serialize($data).'\');';
//  //$script = 'console.log(parent.jQuery(parent));';
//  
//  //drupal_add_js($script, 'inline');
//  
//  return $build_output;
}