<?php

/**
 * @file
 * FacetAPI Field - Field
 *
 * @author amorsent
 */

/**
 * Implementation of hook_field_info().
 */
function facetapi_field_field_info() {
  return array(
    'facetapi_field' => array(
      'label' => t('FacetAPI Field'),
      'description' => t('Store search parameters.'),
    ),
  );
}

/**
 * Implementation of hook_field_settings().
 */
function facetapi_field_field_settings($op, $field) {
  switch ($op) {
    case 'form':
      $searchers = facetapi_get_searcher_info();
      foreach ($searchers as $searcher => $info) {
        $options[$searcher] = $info['label'];
      }
    
      $form = array();
      $form['searcher'] = array(
        '#title' => t('Searcher'),
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => isset($field['searcher']) ? $field['searcher'] : reset(array_keys($options)),
      );
      $form['search_redirect'] = array(
        '#title' => t('Search Redirect'),
        '#description' => t('Redirect matching searches to the corresponding node page.'),
        '#type' => 'checkbox',
        '#default_value' => isset($field['search_redirect']) ? $field['search_redirect'] : TRUE,
      );
      return $form;

    case 'save':
      // Keep a list of facetapi_fields and their redirect setting
      // This allows us to more efficiently handle redirects in hook_init
      $facetapi_fields = variable_get('facetapi_fields', array());
      $facetapi_fields[$field['field_name']] = $field['search_redirect'];
      variable_set('facetapi_fields', $facetapi_fields);
      return array('searcher', 'search_redirect');

    case 'database columns':      
      $columns['data'] = array(
        'type' => 'text',
        'serialize' => TRUE,
        'views' => FALSE,
      );

      return $columns;
  }
}

/**
 * Implementation of hook_field().
 */
function facetapi_field_field($op, &$node, $field, &$items, $teaser, $page) {
  switch ($op) {
    case 'load':
      if (empty($items)) {
        return array();
      }
      foreach ($items as $delta => $item) {
        if (isset($item['data']) && !empty($item['data'])) {
          $item['data'] = unserialize($item['data']);
        }
        // Temporary fix to unserialize data serialized multiple times.
        // Similar to the FileField issue http://drupal.org/node/402860.
        // And the CCK issue http://drupal.org/node/407446.
        while (!empty($item['data']) && is_string($item['data'])) {
          $item['data'] = unserialize($item['data']);
        }
        $items[$delta] = $item;
      }
      
      return array($field['field_name'] => $items);

//    case 'sanitize':
//      if (empty($items)) {
//        return array();
//      }
//      foreach ($items as $delta => $item) {
//        // Get the 'data' column stored by CCK into an array. This is necessary
//        // for Views, which doesn't call the "load" $op and to fix an issue with
//        // CCK double-serializing data.
//        // Similar to the FileField issue http://drupal.org/node/402860.
//        // And the CCK issue http://drupal.org/node/407446.
//        while (!empty($items[$delta]['data']) && is_string($items[$delta]['data'])) {
//          $items[$delta]['data'] = unserialize($items[$delta]['data']);
//        }
//      }
//      break;
  }
}

/**
 * Implementation of hook_content_is_empty().
 */
function facetapi_field_content_is_empty($item, $field) {
  return empty($item['data']);
}
