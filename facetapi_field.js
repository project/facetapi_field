/**
 * FacetAPI Field iframe behavior
 */
Drupal.behaviors.facetapi_field = function(context) {
  // On forms containing a faceted search iframe, set a submit handler
  $('.facetapi-field-iframe', context).parents('form').submit(function(){
    // For each faceted search iframe, update it's associated url form field
    // to reflect the current url of the iframe
    $('.facetapi-field-iframe', $(this)).each(function(){
      iframe = $(this);
      url_field = iframe.siblings('.facetapi-field-url');
      iframe_location = iframe.contents().get(0).location.href;
      url_field.val(iframe_location);
    });
  });
}