<?php

/**
 * @file
 * FacetAPI Field - Formatter
 *
 * @author amorsent
 */

/**
 * Implementation of hook_field_formatter_info().
 */
function facetapi_field_field_formatter_info() {
  return array(
    'default' => array(
      'label' => t('Search Results'),
      'field types' => array('facetapi_field'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
  );
}

/* ************************************************************************
 * Formatter Theme Functions
 * ************************************************************************/

/**
 * Theme function for 'results' FacetAPI Field formatter.
 */
function theme_facetapi_field_formatter_default($element) {
  // When the build mode is 'token' we'll just return nothing
  // This avoids unnecessarily building everything everytime
  // Token wants to know the node title.
  if ($element['#node']->build_mode === 'token') {
    return '';
  }

  // Don't run a search if the field is empty.
  // This avoids triggering the facets
  // @TODO: Make this configurable
  $field = content_fields($element['#field_name']);
  if(facetapi_field_content_is_empty($element['#item'], $field)) {
    return '';
  }

//  $field = content_fields($element['field_name'], $element['type_name']);
//  $searcher = $field['searcher'];
  
  // This is specific to apache solr but in necessary to bootstrap the search facets
  // @todo: Does facetapi provide a more general way to do this ?
  $page_id = 'core_search';
  $search_page = apachesolr_search_page_load($page_id);
  // revise search base path to the current url
  //$search_page['search_path'] = implode('/', array('facetapi-field', $type_name, $field_name, $delta)); 
  $conditions = apachesolr_search_conditions_default($search_page);
  
  // Override f with field values
  $conditions['f'] = $element['#item']['data'];
  // @todo: I'd rather not set this, but it seems that without it the search remains tutally unfiltered
  $_GET['f'] = $element['#item']['data'];
  
  $results = apachesolr_search_search_results($keys, $conditions, $search_page);

  // Give modules a chance to intervene.
  drupal_alter('facetapi_field_results', $results, $element);
  
  // Build our page and allow modification.
  $build_results = apachesolr_search_search_page_custom($results, $search_page, $build);

  $build_output = NULL;
  foreach($build_results as $build_result) {
    $build_output .= $build_result;
  }
  return $build_output;
}