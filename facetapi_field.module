<?php

/**
 * @file
 * FacetAPI Field - Module file.
 *
 * A FacetAPI Field stores faceted search parameters.
 *
 * @author amorsent
 */

// FacetAPI Field API hooks should always be available.
require_once dirname(__FILE__) . '/facetapi_field.field.inc';
require_once dirname(__FILE__) . '/facetapi_field.widget.inc';
require_once dirname(__FILE__) . '/facetapi_field.formatter.inc';

/* ************************************************************************
 * Drupal Hooks
 * ************************************************************************/

/**
 * Implementation of hook_theme().
 */
function facetapi_field_theme() {
  return array(
    'facetapi_field_widget' => array(
      'arguments' => array('element' => NULL),
    ),
    'facetapi_field_widget_item' => array(
      'arguments' => array('element' => NULL),
    ),
    'facetapi_field_formatter_default' => array(
      'arguments' => array('element' => NULL),
    ),
  );
}

/**
 * Implementation of hook_theme_registry_alter().
 *
 * Prepend our module path to the theme registry entry for theme('page').
 */
function facetapi_field_theme_registry_alter(&$theme_registry) {
  if (isset($theme_registry['page']) && isset($theme_registry['page']['theme paths'])) {
    $module_path = drupal_get_path('module', 'facetapi_field');
    array_unshift($theme_registry['page']['theme paths'], $module_path);
    array_unshift($theme_registry['page']['preprocess functions'], 'facetapi_field_pre_preprocess_page');
  }
}

/**
 * Implementation of FAPI hook_elements().
 */
function facetapi_field_elements() {
  return array(
    'facetapi_field_widget' => array(
      '#input'   => TRUE,
      '#columns' => array('data'),
      '#delta'   => 0,
      '#process' => array('facetapi_field_widget_process'),
    ),
  );
}

/**
 * Implementation of hook_menu().
 */
function facetapi_field_menu() {
  $items = array();

  $items['facetapi-field/%/%/%'] = array(
    'page callback' => 'facetapi_field_iframe',
    'page arguments' => array(1, 2, 3),
    'access callback' => 'facetapi_field_edit_access',
    'access arguments' => array(1, 2),
    'type' => MENU_CALLBACK,
    'file' => 'facetapi_field.pages.inc',
  );

  return $items;
}

/**
 * Implementation of hook_init().
 */
function facetapi_field_init() {
  if (isset($_GET['f']) && arg(0) != 'facetapi-field'){
    $facetapi_fields = variable_get('facetapi_fields', array());
    
    // For FacetAPI Fields with search redirect enabled, we'll check for a matching search.
    foreach ($facetapi_fields as $field_name => $search_redirect) {
      if ($search_redirect) {
        // Get a normalized version of the search parameters
        $f = $_GET['f'];
        sort($f);
        $f = serialize($f);
        
        // Get db info for field and look for a matching value
        $field = content_fields($field_name);
        $db_info = content_database_info($field);
        $db_table = $db_info['table'];
        $db_field = $db_info['columns']['data']['column'];
        $nid = db_result(db_query('SELECT nid FROM {' . $db_table . '} WHERE ' . $db_field . " = '%s'", $f));
        
        // If found, redirect to the node.
        if($nid) {
          drupal_goto('node/'.$nid);
        }
      }
    }
  }
}


/* ************************************************************************
 * Template preprocess functions
 * ************************************************************************/

/**
 * Early page preprocess
 *
 * Suppress a few things for the facetapi-field iframe page
 */
function facetapi_field_pre_preprocess_page(&$vars) {
  if (arg(0) == 'facetapi-field') {
    // Prevent template_preprocess_page from rendering blocks
    $vars['show_blocks'] = FALSE;
    
    // Disable admin_menu, admin module output and similar modules, which
    // is something child windows don't need.
    module_invoke_all('suppress');  
  }
}

/**
 * Normal page preprocess
 *
 * Override the template file for the facetapi-field iframe page
 */
function facetapi_field_preprocess_page(&$vars) {
  if (arg(0) == 'facetapi-field') {
    if (!isset($vars['template_files'])) {
      $vars['template_files'] = array();
    }
    $vars['template_files'][] = 'facetapi_field-page';
  }
}

/* ************************************************************************
 * Access functions
 * ************************************************************************/

/**
 * Access callback for iframe callback.
 */
function facetapi_field_edit_access($type_name, $field_name, $node = NULL) {
  if(!user_access('search content')) return FALSE;
  return content_access('edit', content_fields($field_name, $type_name), NULL, $node);
}
